/*傳入參數:
      itemtype:要求的物件類型名稱
      itemid:要求的物件id    
      itemcfgid:要求的物件config_id
      fetchproperty:是否進行fetch所有的property, 預設值"0"。
        選項：
            5: 取得所有的屬性relationship(將物件類型的Property，in_web_add>0的，改成Inn_Property放入Relationship中)
            4: 取得所有的屬性relationship(將物件類型的Property，in_web_edit>0的，改成Inn_Property放入Relationship中)
            3: 取得含有所有預設值的空物件。
            2: 取得物件本體，不取得Relationship。
            1: 取得所有的屬性relationship(將物件類型的Property，in_web_view>0的，改成Inn_Property放入Relationship中)
            0: 不取得
      mode:搜尋模式：預設direct
            direct:使用id進行搜尋，取得指定id的物件。
            latest:使用config_id進行搜尋，取得物件的最新版本。
            
  回傳帶有Inn_Property relationship(含有value及內容)的物件。
  
做法:
  加入可查詢頁籤(relationshiptype的 in_web_list 要設為>1)
  取關聯 file 的方式回歸標準做法 (in_getRel_N)  
  判斷應加入哪些按鈕

*/

Innovator inn=this.getInnovator();
Innosoft.app _InnoApp = new Innosoft.app(inn);
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string itemType=this.getProperty("itemtype","no_data");
string itemID=this.getProperty("itemid","no_data");
string itemcfgID=this.getProperty("itemcfgid","no_data");
string queryMode=this.getProperty("mode","latest");
string fetch=this.getProperty("fetchproperty","1");
string strLang = "zt";
string aml = "";
Item passengerParams=this.apply("In_Collect_PassengerParam");


Item rst=inn.newItem(); //乘載回傳資料的物件
if(fetch=="3" || fetch=="5"){queryMode="";}

if(itemType=="no_data"){
    return inn.newError("Missing Parameter itemtype= "+itemType);
    
}

switch(queryMode){
    case "direct":
    if(itemID!="no_data"){
            rst=inn.getItemById(itemType,  itemID);
    }else{
        return inn.newError("No \"itemtid\" was provided to run this method under direct mode");
    }
    
    if(rst==null){
        Item itmErr=inn.newError("Requested ID not found in database");
        itmErr.setErrorString("Not A Single Item");
        return itmErr;
    }
    break;
    case "latest":
    if(itemcfgID=="no_data"  && itemID=="no_data" ){
        return inn.newError("No \"itemcfgid\" or \"itemid\" was provided to run this method under latest mode");}
        if(itemcfgID!="no_data"){
            rst=inn.newItem(itemType,"get");
            rst.setProperty("config_id",itemcfgID);
            rst.setProperty("is_current","1");
            rst=rst.apply();    
        }else{
            Item original=inn.getItemById(itemType,itemID);
            if(original.isError() || original.getItemCount()==0){
                return inn.newError(itemType +"  with  id "+itemID +"not found in database");}
            rst=inn.newItem(itemType,"get");
            rst.setProperty("config_id",original.getProperty("config_id")); //故意不給預設值，抓不到直接炸。
            rst.setProperty("is_current","1");
            rst=rst.apply();
        

            }
        break;
            
}


//取得property資料
switch(fetch){
    case "0":
        rst = rst.apply("In_ApplyAfterAppGet");    
        break;
    case "1":
        rst.setProperty("map_to_property","2");
        rst = rst.apply("In_ApplyAfterAppGet");    
        rst=rst.apply("In_Fetch_ItemProperties");
        break;
    case "2":break;
    case "3":  
        rst=inn.newItem(itemType,"get");
        rst=rst.apply("In_Fetch_RelationshipDefaults");
        rst = rst.apply("In_ApplyAfterAppGet");    
        rst=rst.fetchDefaultPropertyValues(true);
        return rst;
    case "4":
        rst.setProperty("map_to_property","2");
        rst.setProperty("sort","in_web_edit");
        rst=rst.apply("In_Fetch_RelationshipDefaults");
        rst = rst.apply("In_ApplyAfterAppGet");    
        rst=rst.apply("In_Fetch_ItemProperties");
        break;
    case "5":  
        Item raw=inn.newItem("ItemType","get");
        raw.setProperty("name",itemType);
        raw=raw.apply();
        rst=inn.newItem(itemType);
        rst.setAttribute("isNew", "1");
        rst.setAttribute("typeId",raw.getID());
        rst.setProperty("id.type",itemType);
        rst.setProperty("map_to_property","2");
        rst.setProperty("sort","in_web_add");
        rst=rst.fetchDefaultPropertyValues(true);
        rst.setProperty("fetch",fetch);
        rst=rst.apply("In_ApplyAfterAppGet");
        rst=rst.apply("In_Fetch_ItemProperties");
        rst.setAttribute("isNew", "1");
        rst=rst.apply("In_Fetch_RelationshipDefaults");
        break;    
    
}

Item typeItem=inn.getItemById("ItemType",rst.getAttribute("typeId",""));

rst.setProperty("inn_itemlabel",typeItem.getProperty("label",""));


//判斷應加入哪些按鈕
//要去抓 Action Type 為 app 的 Action, apply method,獲取簽審頁的html,若回傳空值或找不到代表APP不簽審
rst.setProperty("fetch",fetch);
rst.setProperty("lang",strLang);
aml = "<AML>";
aml += "<Item type='Item Action' action='get' select='related_id(method,on_complete)'>";
aml += "<source_id>" + rst.getAttribute("typeId","") + "</source_id>";
aml += "<related_id>";
aml += "<Item type='Action'>";
aml += "<type>app</type>";
aml += "</Item>";
aml += "</related_id>";		
aml += "</Item></AML>";
Item itmItemActions = inn.applyAML(aml);

if(!itmItemActions.isError())
{
	for(int i=0;i<itmItemActions.getItemCount();i++)
	{
		Item itmItemAction = itmItemActions.getItemByIndex(i);
		string strMethod = itmItemAction.getPropertyItem("related_id").getPropertyAttribute("method","keyed_name"); 
		if(strMethod == "In_AppBtn_Vote" && fetch=="5"){continue;}
		if(strMethod!="")
		{
			Item itmActions = rst.apply(strMethod);
			
			if(itmActions!=null)
			{
				for(int j=0;j<itmActions.getItemCount();j++)
				{
					Item itmAction = itmActions.getItemByIndex(j); 
					rst.addRelationship(itmAction);	
				}    					
			}
		}
	}
}





rst = rst.apply("In_AppendExtraProperties");
int passengerAmt=passengerParams.getItemCount();
for(int b=0;b<passengerAmt;b++){
    Item passenger=passengerParams.getItemByIndex(b);
    rst.setProperty(passenger.getProperty("Inn_ParamLabel",""),passenger.getProperty("Inn_ParamValue",""));
    rst.addRelationship(passenger);    
}



return rst;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Get_SingleItemInfoGeneral' and [Method].is_current='1'">
<config_id>32AC92F23AEA485AB667C685AEE3CDF3</config_id>
<name>In_Get_SingleItemInfoGeneral</name>
<comments>取得要求的物件類型資料，及其屬性資料，levels=1，通常由b.aspx 或 c.aspx呼叫。</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
