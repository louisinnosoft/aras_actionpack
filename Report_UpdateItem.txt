var obj = window.event ? window.event.srcElement : event.srcElement ? event.srcElement : event.target;
var topWnd = aras.getMostTopWindowWithAras(window);
aras.setItemProperty(topWnd.item, obj.name, obj.value);
/*
* If the type field has changed lets set up the report_query, xsl_stylesheet, sample_item
* proerties with templates if they are empty.
*/
if (obj.name == 'type' || obj.name == 'location') {
	var itemTypeNameNd = topWnd.item.selectSingleNode('../../../../name');
	if (aras.getItemProperty(topWnd.item, 'type') == 'itemtype') {
		if (!aras.confirm(aras.getResource('', 'report.standard_template'))) {
			return false;
		}
		if (aras.getItemProperty(topWnd.item, 'location') != 'service') {
			var templateDOM = aras.createXMLDocument();
			if (itemTypeNameNd) {
				templateDOM.loadXML('<Item type=\'' + itemTypeNameNd.text + '\' action=\'get\' select=\'\'/>');
			} else {
				templateDOM.loadXML('<Item type=\'\' action=\'get\' select=\'\'/>');
			}
			if (templateDOM.parseError.errorCode) {
				aras.AlertError(aras.getResource('', 'report.template_err', templateDOM.parseError.reason));
				return false;
			}
			aras.setItemProperty(topWnd.item, 'report_query', templateDOM.xml);
			document.getElementsByClassName('report_query')[0].value = templateDOM.xml;
		} else {
			var template = 'np:type=<xsl:value-of select=\'@type\'/>';
			aras.setItemProperty(topWnd.item, 'report_query', template);
			document.getElementsByClassName('report_query')[0].value = template;
		}
	}
	if (aras.getItemProperty(topWnd.item, 'type') == 'item') {
		if (!aras.confirm(aras.getResource('', 'report.standard_template'))) {
			return false;
		}
		if (aras.getItemProperty(topWnd.item, 'location') != 'service') {
			var templateDOM = aras.createXMLDocument();
			templateDOM.loadXML('<Item type=\'{@type}\' id=\'{@id}\' action=\'get\' select=\'\'/>');
			if (templateDOM.parseError.errorCode) {
				aras.AlertError(aras.getResource('', 'report.template_err', templateDOM.parseError.reason));
				return false;
			}
			aras.setItemProperty(topWnd.item, 'report_query', templateDOM.xml);
			document.getElementsByClassName('report_query')[0].value = templateDOM.xml;
		} else {
			var template = 'np:id=<xsl:value-of select=\'@id\'/>';
			aras.setItemProperty(topWnd.item, 'report_query', template);
			document.getElementsByClassName('report_query')[0].value = template;
		}
		if (aras.getItemProperty(topWnd.item, 'sample_item') === '' && itemTypeNameNd) {
			aras.setItemProperty(topWnd.item, 'sample_item', '<Item type=\'' + itemTypeNameNd.text + '\' id=\'\'/>');
		}
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='Report_UpdateItem' and [Method].is_current='1'">
<config_id>5CF21C73A791456DAD15E3E98F7013E6</config_id>
<name>Report_UpdateItem</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
