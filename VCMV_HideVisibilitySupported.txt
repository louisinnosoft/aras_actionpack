var classPath = aras.getItemProperty(document.item, 'class_path');
if (classPath) {
	var collection = document.getElementsByName('visibility_supported');
	if (collection.length > 0) {
		var classPathField  =  collection[0];
		if (classPathField.style) {
			classPathField.style.display = 'none';
		}
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VCMV_HideVisibilitySupported' and [Method].is_current='1'">
<config_id>9DB0EF5E0FA84C26B3861E94DBBB3AF8</config_id>
<name>VCMV_HideVisibilitySupported</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
