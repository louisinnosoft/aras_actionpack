var userId = CCO.Variables.GetUserID();
var worldIdentityId = Aras.Server.Security.Identity.GetByName("World").Id;
var userIdentity = newItem("User", "VC_GetUserIsAliasIdentity");
userIdentity.setID(userId);
userIdentity = userIdentity.apply();
if (userIdentity.isError())
{
	return userIdentity;
}

var idenities = newItem("Identity", "get");
idenities.setAttribute("orderBy", "name");
idenities.setAttribute("select", "name, id");
var not = idenities.newNOT();
not.setProperty("id", string.Format("'{0}','{1}'", worldIdentityId, userIdentity.getID()));
not.setPropertyCondition("id", "in");
var or = idenities.newOR();
or.setPropertyCondition("classification", "is null");
var secondOr = or.newOR();
var secondNot = secondOr.newNOT();
secondNot.setProperty("classification", "'System','Team'");
secondNot.setPropertyCondition("classification", "in");

return idenities.apply();
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VC_GetIdentExceptTeamSystemOwn' and [Method].is_current='1'">
<config_id>09501103BCFA4C91BE3417D88EA7B72E</config_id>
<name>VC_GetIdentExceptTeamSystemOwn</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
