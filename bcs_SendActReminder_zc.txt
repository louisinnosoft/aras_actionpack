/*
 * Run Server Method
 */

Innovator inn = this.getInnovator();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Aras PLM");
bool PermissionWasSet  = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

try
{
//	//System.Diagnostics.Debugger.Break();
	
	string strActReminderMessageName = "bcs_ActivityReminder_zc";
	string strResult = "";
	
	bcsNotificationLite.Notification _Notification = new bcsNotificationLite.Notification(inn);
	
	strResult = _Notification.SendActReminder(strActReminderMessageName);
	
	return inn.newResult(strResult);
}
catch (Exception ex)
{
	return inn.newError(ex.Message);
}
finally
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
}
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='bcs_SendActReminder_zc' and [Method].is_current='1'">
<config_id>73432A45ADBD4E0F812FE4C0A2F0A87E</config_id>
<name>bcs_SendActReminder_zc</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
