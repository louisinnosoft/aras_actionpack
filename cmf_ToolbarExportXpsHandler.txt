var docEditor = window.cmfContainer.qpWindow.documentEditor;
if (docEditor) {
	docEditor.exportToXps();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_ToolbarExportXpsHandler' and [Method].is_current='1'">
<config_id>814C02EC4C54484CB49FD84A417103EC</config_id>
<name>cmf_ToolbarExportXpsHandler</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
