//MethodTemplateName=CSharpInOut;
throw new Exception( "Can't delete SharePoint document from Innovator" );
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='SPDocument_onDelete' and [Method].is_current='1'">
<config_id>470C00CF1CAE46869D7FEEB8B9CF9D02</config_id>
<name>SPDocument_onDelete</name>
<comments>Returns an error on attempt to delete document from SP</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
