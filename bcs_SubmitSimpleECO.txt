/*
 * 設定方式
 * Workflow Map : BCS Simple ECO Workflow
 * Activity : Auto Verify
 * Server Events : onActive
 */

Innovator inn = this.getInnovator();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Aras PLM");
bool PermissionWasSet  = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

try
{
//	//System.Diagnostics.Debugger.Break();
	
	bcsActionPack.ActionPack _ap = new bcsActionPack.ActionPack(inn);
	Item itmResult = _ap.SubmitSimpleECO(this);
	
	if (itmResult.isError()) return itmResult;
}
catch (Exception ex)
{
	return inn.newError(ex.Message);
}
finally
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='bcs_SubmitSimpleECO' and [Method].is_current='1'">
<config_id>E06C5ECAAF1A4FD8A0A826F9495CAE53</config_id>
<name>bcs_SubmitSimpleECO</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
