if(document.item)
{
  var isMilestone = top.aras.getItemProperty(document.item, 'is_milestone') == '1' ? true : false;
  if(isMilestone)
  {
    var start = srcElement.value;
    var finish = top.aras.getItemProperty(document.item,'date_due_sched');
    if(start!=finish)
    {
      handleItemChange("date_due_sched",start);

      var inputs = document.getElementsByTagName('INPUT');
      for(var i=0;i<inputs.length;i++)
      {
        var inp = inputs[i];
        if(inp.name=='date_due_sched')
        {
          inp.value = start;
          break;
        }
      }
    }
  }
}
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='onchangeStartDate' and [Method].is_current='1'">
<config_id>466EDACB707E4F9CA4C8F3D9502400B7</config_id>
<name>onchangeStartDate</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
