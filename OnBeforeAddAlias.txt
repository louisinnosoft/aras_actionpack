var userKeyedName = aras.getItemProperty(item, 'keyed_name');
var identityKeyedName = getRelatedItemProperty(relationshipID, 'keyed_name');
var isAlias = getRelatedItemProperty(relationshipID, 'is_alias');
var needsWarning = false;

if (isAlias == '1') {
	userKeyedName = userKeyedName.replace(/^\s*/, '').replace(/\s*$/, '');
	identityKeyedName = identityKeyedName.replace(/^\s*/, '').replace(/\s*$/, '');
	if (userKeyedName != identityKeyedName) {
		needsWarning = true;
	}
}
if (needsWarning) {
	if (!aras.confirm(aras.getResource('', 'com.aras.innovator.core.on_alias_add_warning', identityKeyedName, userKeyedName))) {
		deleteRelationship();
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='OnBeforeAddAlias' and [Method].is_current='1'">
<config_id>171DD80ABE7043CA87F0BC5A5B33DB48</config_id>
<name>OnBeforeAddAlias</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
