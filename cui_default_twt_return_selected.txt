var control = inArgs.control;
if (control && control['_item_Experimental'] && control['_item_Experimental'].ownerDocument) {
	var context = control['_item_Experimental'].ownerDocument.defaultView;
	if (context && context.onSaveAndCloseCommand) {
		return context.onSaveAndCloseCommand();
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_twt_return_selected' and [Method].is_current='1'">
<config_id>A55EE100F18D4B54A3A58C875056611B</config_id>
<name>cui_default_twt_return_selected</name>
<comments>Structure Toolbar Return Selected Button</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
