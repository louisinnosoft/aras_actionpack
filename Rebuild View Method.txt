/*version:5*/
var viewNd =  this.node.selectSingleNode('self::Item[@type=\'View\']');
if (!viewNd) {
	alert(aras.getResource('', 'imports_core.item_is_not_view'));
	return false;
}
if (aras.isTempEx(viewNd)) {
	alert(aras.getResource('', 'imports_core.view_is_not_saved'));
	return false;
}

var viewID = viewNd.getAttribute('id');
var win = window;//will be used to refresh relationships grid UI
var refreshFlg = false;//indicate if refresh may success
var itemTypeId = aras.getItemProperty(viewNd, 'source_id');
var res = aras.rebuildView(viewID);
try {
	win = aras.uiFindWindowEx(itemTypeId);
	if (!win || win.closed) {
		refreshFlg = false;
		win = window;
	} else {
		refreshFlg = true;
	}
	refreshFlg = refreshFlg && (win.document.frames.relationships !== undefined);
	if (refreshFlg) {
		win = win.document.frames.relationships;
		if (win.item && win.isEditMode !== undefined) {
			if (win.isEditMode) {
				win.setEditMode(win.item);
			} else {
				win.setViewMode(win.item);
			}
		}
	}
} catch (excep) {}
if (res) {
	aras.AlertSuccess(aras.getResource('', 'imports_core.view_update_success'));
} else {
	aras.AlertError(aras.getResource('', 'imports_core.view_update_failed'));
}
return true;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='Rebuild View Method' and [Method].is_current='1'">
<config_id>3EFD6D3ED6D04072858F6BEC7C00280D</config_id>
<name>Rebuild View Method</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
