var itemId = inArgs.itemId;
var itemTypeName = inArgs.itemTypeName;
var itemTypeId = inArgs.typeId;

if (!aras.getPermissions('can_get', itemId, itemTypeId, itemTypeName)) {
	aras.AlertWarning(aras.getResource('', 'ssvc.secure_message.no_get_permission'));
	return;
}

if (execInTearOffWindow(itemId)) {
	return;
}

aras.uiShowItem(itemTypeName, itemId);

function execInTearOffWindow(id) {
	var itemWindow = aras.uiFindWindowEx(id);

	if (!itemWindow) {
		return false;
	}

	if (aras.isWindowClosed(itemWindow)) {
		aras.uiUnregWindowEx(id);
		return true;
	}

	if (itemWindow.name === 'work') {
		return true;
	}

	aras.browserHelper.setFocus(itemWindow);

	var tearoffMC = itemWindow.tearOffMenuController;

	if (!tearoffMC || !tearoffMC.onClickMenuItem) {
		return true;
	}

	var execResult = tearoffMC.onClickMenuItem('view');

	if (!execResult || !execResult.result) {
		return true;
	}

	return execResult;
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VC_AM_ViewThisVersionItem' and [Method].is_current='1'">
<config_id>1EA2AC86C5764E4D89FC7ED3EA48A2E7</config_id>
<name>VC_AM_ViewThisVersionItem</name>
<comments>AM - is abbreviation of Actions Menu</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
