
	Innovator innovator = this.getInnovator();
	string userId = innovator.getUserID();
	string userAliasIdentityId = CCO.Utilities.GetIdentityIdByUserId(userId);
	this.setProperty("owned_by_id", userAliasIdentityId);
	return this;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='SetUserAliasIdentityToOwnedBy' and [Method].is_current='1'">
<config_id>526172B2ECF4470CB781F1ACE49A408D</config_id>
<name>SetUserAliasIdentityToOwnedBy</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
