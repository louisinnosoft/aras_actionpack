//System.Diagnostics.Debugger.Break();
//in_GetAssignedTasks
Innovator inn =this.getInnovator();
XmlDocument inDom = new XmlDocument();
XmlDocument outDom = new XmlDocument();
string user_id =this.getProperty("UserID", "");
string view_mode =this.getProperty("ViewMode", "'Both'");   
string workflow_tasks_flag =this.getProperty("WorkflowTasksFlag", "0");
string project_tasks_flag = this.getProperty("ProjectTasksFlag", "0");
string action_tasks_flag  = this.getProperty("ActionTasksFlag", "0");


inDom.LoadXml("<params><inBasketViewMode>" + view_mode + "</inBasketViewMode><workflowTasks>" + workflow_tasks_flag + "</workflowTasks><projectTasks>" + project_tasks_flag + "</projectTasks><actionTasks>" + action_tasks_flag + "</actionTasks><userID>" + user_id + "</userID></params>");



CCO.Utilities.ApplySoapAction("GetAssignedTasks", inDom,  outDom, true);

Item result = inn.newItem();
result.loadAML(outDom.OuterXml);

return result;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GetAssignedTasks' and [Method].is_current='1'">
<config_id>D1B8D53995844DF88FF96214C6642507</config_id>
<name>In_GetAssignedTasks</name>
<comments>Add By Inno</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
