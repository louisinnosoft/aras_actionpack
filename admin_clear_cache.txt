    if (!top.aras.clearCache()) top.aras.AlertError (top.aras.getResource("", "imports_core.clear_cache_failed"));
    else top.aras.AlertError (top.aras.getResource("", "imports_core.clear_cache_success"));
    
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='admin_clear_cache' and [Method].is_current='1'">
<config_id>6D26462C80174CC4952DF0ABDF4D5B14</config_id>
<name>admin_clear_cache</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
