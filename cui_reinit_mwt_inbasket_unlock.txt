if (inArgs.isReinit) {
	if (Object.getOwnPropertyNames(inArgs.eventState).length === 0) {
		var states = aras.evalMethod('cui_reinit_calculate_states', '', {eventType: inArgs.eventType});
		var keys = Object.keys(states);
		for (var i = 0; i < keys.length; i++) {
			inArgs.eventState[keys[i]] = states[keys[i]];
		}
	}

	var topWindow = aras.getMostTopWindowWithAras(window);
	var workerFrame = topWindow.work;
	if (workerFrame && workerFrame.currQryItem) {
		var queryItem = workerFrame.currQryItem.getResult();
		var grid = workerFrame.grid;
		if (grid) {
			var itemId = grid['getSelectedId_Experimental']();
			var itemNd = queryItem.selectSingleNode('Item[@id="' + itemId + '"]') || aras.getFromCache(itemId);
			var itemTypeName = aras.getItemTypeName(aras.getItemProperty(itemNd, 'itemtype'));
			return {'cui_disabled': !(inArgs.eventState.isUnlock && !isFunctionDisabled(itemTypeName, 'Unlock'))};
		}
	}
}
return {};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_reinit_mwt_inbasket_unlock' and [Method].is_current='1'">
<config_id>B160D1E9209B47BFB08D9AA5FF6C4565</config_id>
<name>cui_reinit_mwt_inbasket_unlock</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
