var aml = '<AML><Item type=\'Method\' action=\'cmf_GetViews\'><item_type_id>' + inArgs.itemTypeId + '</item_type_id></Item></AML>';
var response = aras.applyAML(aml);
var xmldom = aras.createXMLDocument();
xmldom.loadXML(response);
var docTypeNode = xmldom.selectSingleNode('Result/Item[@type=\'cmf_ContentType\']');

var spreadsheetViews = xmldom.selectNodes('Result/Item[@type=\'cmf_TabularView\']');
var viewCollection = [];
var emptyObject = {text: ''};
for (var i = 0; i < spreadsheetViews.length; i++) {
	viewCollection.push({
		id: (spreadsheetViews[i].selectSingleNode('id') || emptyObject).text,
		name: (spreadsheetViews[i].selectSingleNode('name') || emptyObject).text,
		label: (spreadsheetViews[i].selectSingleNode('label') || emptyObject).text,
		docTypeId: docTypeNode.getAttribute('id'),
		viewId: (spreadsheetViews[i].selectSingleNode('id') || emptyObject).text,
		itemId: inArgs.itemId
	});
}
return viewCollection;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_GetViewsForCommandBars' and [Method].is_current='1'">
<config_id>5989399FE026423A97DF56C82C484873</config_id>
<name>cmf_GetViewsForCommandBars</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
