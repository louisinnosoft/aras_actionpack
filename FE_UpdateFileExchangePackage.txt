string teamId = this.getProperty("team_id");
string managedById = this.getProperty("managed_by_id");
string ownedById = this.getProperty("owned_by_id");

Item packageFile = this.newItem("FileExchangePackageFile", "get");
packageFile.setAttribute("select", "related_id");
Item query = this.newItem("FileExchangePackage", "get");
query.setID(this.getID());
query.addRelationship(packageFile);
query = query.apply();
if (query.isError())
	return query;	
query = query.getRelationships("FileExchangePackageFile");

for (int i = 0; i < query.getItemCount(); i++)
{
	Item pkg_file = query.getItemByIndex(i);
	pkg_file = pkg_file.getRelatedItem();
		
	XmlDocument body = new XmlDocument();
	body.XmlResolver = null;
	body.LoadXml("<body />");
	body.DocumentElement.SetAttribute("fileId", pkg_file.getID());
	body.DocumentElement.SetAttribute("teamId", teamId);
	body.DocumentElement.SetAttribute("managedById", managedById);
	body.DocumentElement.SetAttribute("ownedById", ownedById);
	
	Innovator innovator = this.getInnovator();
	innovator.applyMethod("FE_SetFilePackageProperties", body.OuterXml);
}
return query;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='FE_UpdateFileExchangePackage' and [Method].is_current='1'">
<config_id>2560D61A725C402087A337323A52A176</config_id>
<name>FE_UpdateFileExchangePackage</name>
<comments>Update file exchange package</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
