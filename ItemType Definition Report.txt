' Query for the itemtype, using a where clause to avoid getting it from the cache
Dim queryItem As Item = Me.newItem("ItemType","get")
queryItem.setAttribute("where","[ITEMTYPE].id='"+Me.getID()+"'")
queryItem.setAttribute("levels","1")
Dim thisItem As Item = queryItem.apply()
If thisItem.isError() Then 
 Return Me.getInnovator().newError("Error running ItemType query")
End If

' Convert the class_structure property to actual XML so the stylesheet can work on it
Dim classDom As XmlDocument = Me.newXMLDocument()
Try
 classDom.loadXML(thisItem.getProperty("class_structure"))
 If Not classDom.selectSingleNode("/class") Is Nothing Then
  thisItem.setProperty("class_structure","")
  Dim newNode As XmlNode = thisItem.dom.ImportNode(classDom.selectSingleNode("/class"),True)
  thisItem.node.selectSingleNode("class_structure").appendChild(newNode)
 End If
Catch e As Exception
End Try
Return thisItem
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='ItemType Definition Report' and [Method].is_current='1'">
<config_id>712C6A183FDE4B17AE1EBB1018D8418F</config_id>
<name>ItemType Definition Report</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
