/*
目的:強化merge效果,如果物件不存在,會轉成 add, 且指定id,id的指定次序為config_id,id,隨機
做法:
*/
string strMethodName = "in_merge";
//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
string strError = "";
string strUserId = inn.getUserID();
string strIdentityId = inn.getUserAliases();

_InnH.AddLog(strMethodName,"MethodSteps");
Item itmR = null;

try
{
	itmR = inn.newItem(this.getType());
	itmR.setAction("get");
	itmR.setAttribute("where",this.getAttribute("where"));
	itmR = itmR.apply();	
	if(itmR.isError())
	{
		//代表該條件找不到資料,則需要變更action為add,並且指定 id
		this.setAction("add");
		this.removeAttribute("where");
		string id = this.getProperty("config_id","");
		if(id=="")
			id=this.getProperty("id","");
		if(id=="")
			id=this.getAttribute("id","");
		if(id!="")
			this.setID(id);
		itmR = this.apply();
	}
	else
	{
		this.setAction("merge");
		itmR = this.apply();
	}

}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

	string errMsg = "";
    if(ex.GetType().ToString()=="Aras.Server.Core.InnovatorServerException")
    {
        Aras.Server.Core.InnovatorServerException e = (Aras.Server.Core.InnovatorServerException)ex;
        XmlDocument errDom = new XmlDocument();
        e.ToSoapFault(errDom); 
        errMsg = errDom.GetElementsByTagName("faultstring")[0].InnerText;
        if(errMsg=="")
            errMsg = errDom.GetElementsByTagName("faultstring")[0].InnerXml;
    }
    else
    {
        errMsg=ex.Message;
    }
	
	if(strError=="")
	{
		strError = errMsg + "\n";

		if(ex.Source=="IOM")
		{
			//代表這是非預期的錯誤
			if(aml!="")
				strError += "無法執行AML:" + aml  + "\n";

			if(sql!="")
				strError += "無法執行SQL:" + sql  + "\n";
		}
	}	
	string strErrorDetail="";
	strErrorDetail = strMethodName + ":" + strError; 
	_InnH.AddLog(strErrorDetail,"Error");
	//strError = strError.Replace("\n","</br>");
	
	throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='in_merge' and [Method].is_current='1'">
<config_id>434B3B07B7D74EA8BE3875B20A8ADE35</config_id>
<name>in_merge</name>
<comments>強化merge效果,如果物件不存在,會轉成 add, 且指定id 為 config_id</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
