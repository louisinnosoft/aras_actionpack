var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
var itemNode = workerFrame.currQryItem.getResult().selectSingleNode('Item[@id="' + inArgs.rowId + '"]');
if (itemNode) {
	var label = aras.getNodeElement(workerFrame.currItemType, 'label') || aras.getNodeElement(workerFrame.currItemType, 'name');
	var dialogParams = {
		aras: aras,
		item: itemNode,
		itemType: workerFrame.currItemType,
		title: aras.getResource('', 'propsdialog.item_type_label__item_keyed_name__properties', label, aras.getKeyedNameEx(itemNode))
	};
	var options = {
		dialogWidth: 225,
		dialogHeight: 350,
		resizable: true
	};

	var dialog = workerFrame.ArasModules.Dialog.show('iframe', Object.assign({
		content: 'propsDialog.html'
	}, dialogParams, options));
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_pmig_properties' and [Method].is_current='1'">
<config_id>C7ADE819235D426C9DB009FA962491FC</config_id>
<name>cui_default_pmig_properties</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
