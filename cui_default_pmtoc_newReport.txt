var topWindow = aras.getMostTopWindowWithAras(window);
var treeFrame = topWindow.tree;
if (treeFrame && treeFrame.ItemTypeInToc) {
	var itInToc = new treeFrame.ItemTypeInToc(inArgs.rowId);
	itInToc.createNewReport();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_pmtoc_newReport' and [Method].is_current='1'">
<config_id>1E5C37414D514ECC88DA2C5641EBBFB4</config_id>
<name>cui_default_pmtoc_newReport</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
