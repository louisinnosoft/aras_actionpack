Item documentTypeQuery = this.newItem("cmf_ElementType", "get");
documentTypeQuery.setID(this.getProperty("source_id"));
documentTypeQuery.setAttribute("select", "source_id");
documentTypeQuery = documentTypeQuery.apply();
string documentTypeId = documentTypeQuery.getProperty("source_id");

if (!RequestState.Contains(documentTypeId))
{
    return this.getInnovator().newError("It's impossible to save certain Element Type or Property Type. Instead save entire Content Type item.");
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_ElementBindingAdd' and [Method].is_current='1'">
<config_id>BA7FA3A6F1304255B97916498D9578F1</config_id>
<name>cmf_ElementBindingAdd</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
