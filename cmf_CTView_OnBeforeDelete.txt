Innovator inn = this.getInnovator();
var contentTypeViewId = this.getAttribute("id");
var amlQuery = String.Format("<AML><Item type='cmf_ContentTypeView' action='get' select='related_id' id='{0}'/></AML>", contentTypeViewId);
Item result = inn.applyAML(amlQuery);
if(result.isError()){
    return result;
}
result.getPropertyItem("related_id").apply("delete");
if(result.isError()){
    return result;
}
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_CTView_OnBeforeDelete' and [Method].is_current='1'">
<config_id>A223A209B32B43DCBD94F39ACB9523AE</config_id>
<name>cmf_CTView_OnBeforeDelete</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
