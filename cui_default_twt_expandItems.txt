var control = inArgs.control;
if (control && control['_item_Experimental'] && control['_item_Experimental'].ownerDocument) {
	var context = control['_item_Experimental'].ownerDocument.defaultView;
	if (context && context.onExpandAllCommand) {
		return context.onExpandAllCommand();
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_twt_expandItems' and [Method].is_current='1'">
<config_id>69D4809F7BA74EEF8FE934CC285C7F57</config_id>
<name>cui_default_twt_expandItems</name>
<comments>Structure Toolbar Expand Structure Items Button</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
