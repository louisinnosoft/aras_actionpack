/*
 * Item Type : Activity2 Deliverable
 * Server Events : onAfterAdd
 */

Innovator inn = this.getInnovator();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Aras PLM");
bool PermissionWasSet  = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Aras.Server.Security.Identity historyIdentity = Aras.Server.Security.Identity.GetByName("History Daemon");
Boolean PermissionWasSet2 = Aras.Server.Security.Permissions.GrantIdentity(historyIdentity);

try
{
//	//System.Diagnostics.Debugger.Break();

	bcsActionPack.ActionPack _ap = new bcsActionPack.ActionPack(inn);
	Item itmResult = _ap.UpdateDeliverableTeam(this, "Edit");
	
	if (itmResult.isError()) return itmResult;
}
catch (Exception ex)
{
	return inn.newError(ex.Message);
}
finally
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

	if (PermissionWasSet2) Aras.Server.Security.Permissions.RevokeIdentity(historyIdentity);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='bcs_UpdateDeliverableTeamSave' and [Method].is_current='1'">
<config_id>B2BD40A77A56417EA2AB4365E0A92EBD</config_id>
<name>bcs_UpdateDeliverableTeamSave</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
