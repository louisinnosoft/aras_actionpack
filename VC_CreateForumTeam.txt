	var forumType = this.getProperty("forum_type");
	if (forumType != "MyBookmarks")
	{
		var forumId = this.getID();
		var teamId = this.getProperty("team_id");
		if (string.IsNullOrEmpty(teamId))
		{
			Aras.Server.Security.Identity admin = null;
			bool permsWasSet = false;
			try
			{
				admin = Aras.Server.Security.Identity.GetByName("Super User");
				permsWasSet = Aras.Server.Security.Permissions.GrantIdentity(admin);
				var teamItem = this.newItem("Team", "add");
				teamItem.setProperty("name", forumId);
				teamItem = teamItem.apply();
				if (teamItem.isError())
				{
					return teamItem;
				}
				teamId = teamItem.getID();
			}
			finally
			{
				if (permsWasSet)
				{
					Aras.Server.Security.Permissions.RevokeIdentity(admin);
				}
			}
		}
		this.setProperty("team_id", teamId);
	}
	
	return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VC_CreateForumTeam' and [Method].is_current='1'">
<config_id>BF5211467ABC405982B6DC23CB5064C2</config_id>
<name>VC_CreateForumTeam</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
