var filter = {};
filter['source_id'] = {
	filterValue: aras.getItemProperty(inArgs.itemContext, 'source_id'),
	isFilterFixed: true
};
return filter;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_BindingElementTypeFilter' and [Method].is_current='1'">
<config_id>D9BA201C70BA4FDDB544FCEAB409420D</config_id>
<name>cmf_BindingElementTypeFilter</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
