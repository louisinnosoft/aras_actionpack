/*
 * Lifrcycle : NPR Lifecycle
 * Life Cycle Transition : to In Review
 * Server Events : Before
 */

Innovator inn = this.getInnovator();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Aras PLM");
bool PermissionWasSet  = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

try
{
//	//System.Diagnostics.Debugger.Break();
	
	bcsActionPack.ActionPack _ap = new bcsActionPack.ActionPack(inn);
	Item itmResult = _ap.NPRPromoteToInReview(this);
	
	if (itmResult.isError()) return itmResult;
}
catch (Exception ex)
{
	return inn.newError(ex.Message);
}
finally
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='bcs_NPRPromoteToInReview' and [Method].is_current='1'">
<config_id>162A5148257449BD85BA4BAC6F65EC50</config_id>
<name>bcs_NPRPromoteToInReview</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
