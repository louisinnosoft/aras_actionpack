  myCCO = CCO;
  
  return main();
}

private Aras.Server.Core.CallContext myCCO;

private string getLanguageCriteria(string propName, string propValue)
{
  string res = "[" + propName + "]";
  if (propValue == null || propValue.Length == 0)
  {
    res += " IS NULL";
  }
  else
  {
    res += "='" + propValue.Replace("'", "''")+ "'";
  }

  return res;
}

private Item main()
{
  if (this.isEmpty())
  {
    return this;
  }

  Aras.Server.Core.InnovatorDatabase conn = myCCO.Variables.InnDatabase;

  string code   = this.getProperty("code");
  string suffix = this.getProperty("suffix");

  string codeCriteria   = getLanguageCriteria("code", code);
  string suffixCriteria = getLanguageCriteria("suffix", suffix);

  string sql = String.Format(
    "SELECT [CODE], [SUFFIX] " +
    "FROM " + conn.GetTableName("Language") +
    " WHERE [ID]<>'{0}' AND ({1} OR {2}) /*(IR-012352) Core_CheckLanguageUniqueness marker*/",
    this.getID().Replace("'", "''"),
    codeCriteria, suffixCriteria);

  string errorDescription = "";
  Aras.Server.Core.InnovatorDataSet rs = conn.ExecuteSelect(sql);
  if (!rs.Eof)
  {
  	string codeInDB   = (string)rs["code", null];
  	string suffixInDB = (string)rs["suffix", null];
  	
    if (codeInDB == code && suffixInDB == suffix)
      errorDescription = String.Format("Language with Code ({0}) and Suffix ({1}) already exists.", code, suffix);
    else if (codeInDB == code)
      errorDescription = String.Format("Language with Code ({0}) already exists.", code);
    else
      errorDescription = String.Format("Language with Suffix ({0}) already exists.", suffix);
  }

  if (errorDescription.Length > 0)
    return this.getInnovator().newError(errorDescription);
  else
    return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='Core_CheckLanguageUniqueness' and [Method].is_current='1'">
<config_id>5DA69370408C4BDD978B3EFFF228DFCE</config_id>
<name>Core_CheckLanguageUniqueness</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
