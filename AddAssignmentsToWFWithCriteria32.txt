'(c) Copyright by IRES Corporation (Aras GC), 2009-2020.
'Create Date: 2009/02/16, Creator: Scott
'Modified Date: 2009/11/11, Modified by Scott
'Modified Date: 2009/11/25, Modified by Scott
'2009/11/11: 新增 Assignment Type = Item Field
'2009/11/25: 修正會重複填入審核者
'2010/11/02：修正 Assignee 如果填入系統預設的 Identity (Creator, Manager, Owner) 會出現比對不到

'System.Diagnostics.Debugger.Break() 

Dim inn As Innovator = Me.getInnovator()
Dim err As Item
Dim wfm_name As String       'Workflow Map name
Dim at_name As String           'Activity Template Name
Dim item_type As String         'The item type of controlled item
Dim item_id As String            'The id of controlled item
Dim item_type_id As String    'The id of item type of controlled item
Dim identity As Item                'Creator's user identity
Dim creator_id As String         'Creator ID

'******************************************************************************************************************
'1.找出 Workflow Map & Activity Template 名稱
Dim wfp As Item = inn.newItem("Workflow Process", "get")
Dim wfpa As Item = inn.newItem("Workflow Process Activity", "get")
wfpa.setProperty("related_id", Me.getID())
wfp.addRelationship(wfpa)
Dim r_wfp As Item = wfp.apply()

If r_wfp.isError() Then
	err = inn.newError(r_wfp.getErrorString())
	Return err
End If

Dim wfm As Item = inn.newItem("Workflow Map", "get")
wfm.setID(r_wfp.getProperty("copied_from_string"))
Dim r_wfm As Item = wfm.apply()

If r_wfm.isError() Then
	err = inn.newError(r_wfm.getErrorString())
	Return err
End If

wfm_name = r_wfm.getProperty("name")
at_name = Me.getProperty("name")

'******************************************************************************************************************
'2-1.取出 Controlled Item (表單)，並記錄其 ItemType
Dim controlledItem As Item
Dim ca, r_ca As Item

controlledItem = Me.newItem(Me.GetType(),"Get My Controlled Item")
controlledItem.setID(Me.getID())
controlledItem = controlledItem.apply()

If controlledItem.isError() Then
	err = inn.newError(controlledItem.getErrorString())
	Return err
End If

item_type = controlledItem.GetType()
item_id = controlledItem.getID()

'2-2.取出表單的 Creator
ca = inn.newItem("Alias", "get")
ca.setAttribute("select", "id, related_id")
ca.setProperty("source_id", controlledItem.getProperty("created_by_id")) 
r_ca = ca.apply()

If r_ca.isError() Then
	err = inn.newError(r_ca.getErrorString())
	Return err
End If

identity = r_ca.getRelatedItem().getItemByIndex(0)
creator_id = identity.getID()

'2-3.取得 The id of item type of controlled item
Dim it As Item = inn.getItemByKeyedName("ItemType", item_type)

If it.isError() Then
	err = inn.newError(it.getErrorString())
	Return err
End If

item_type_id = it.getID()

'******************************************************************************************************************
'3.取出 Assignment Matrix 名稱
Dim var As Item = inn.newItem("Variable", "get")
var.setProperty("name", "Current Assignment Matrix")
Dim r_var As Item = var.apply()

If r_var.isError() Then
	err = inn.newError(r_var.getErrorString())
	Return err
End If

Dim am_name As String = r_var.getProperty("value")

'******************************************************************************************************************
'4.取出符合條件的 Assignment Activity
Dim am As Item = inn.newItem("Assignment Matrix", "get")
am.setProperty("name", am_name)
Dim r_am As Item = am.apply()

If r_am.isError() Then
	err = inn.newError(r_am.getErrorString())
	Return err
End If

Dim aas As Item = inn.newItem("Assignment Activity", "get")
aas.setProperty("source_id", r_am.getID())
aas.setProperty("workflow_map", wfm_name)
aas.setProperty("activity_template", at_name)

Dim or_item1 As Item = aas.newOR()
Dim criteria As Item = inn.newItem("Criteria", "get")
criteria.setProperty("item_type", item_type_id)
or_item1.setPropertyItem("criteria", criteria)

Dim or_item2 As Item = or_item1.newOR()
or_item2.setProperty("criteria", "")
or_item2.setPropertyCondition("criteria", "is null")

Dim r_aas As Item = aas.apply()
Dim count As Integer = r_aas.getItemCount()

If count = -1 Then
	err = inn.newError(r_aas.getErrorString())
	Return err
End If

'******************************************************************************************************************
'5.Assign all assignments
Dim i, j, num As Integer
Dim aa As Item    'Assignment Activity
Dim assignments, r_assignments As Item
Dim assignment As Item
'Dim r_assignment As Item
Dim ass_type As String  'Assignment Type：Manager、Identity、Field、Previous Activity Manager
Dim level As Integer
Dim ass, r_ass As Item    'Activity Assignment
Dim act, r_act As Item
Dim org As Item
Dim r_org As Item = Nothing
Dim oi As Item
Dim result As item
Dim level_mgr_id As String
Dim manager_id As String = ""
Dim field_name As String
Dim item_field_name As String
Dim property_item As Item
Dim r_property_item As Item
Dim itm_data_type As String
Dim itm_itemtype_id As String
Dim r_it As Item
Dim itm_itemtype_name As String
Dim itm_id As String
Dim itm As Item
Dim r_itm As Item
Dim exist_flag As Item
Dim assignee_id As String
Dim identity_type As Item

For i=0 To count-1
	aa = r_aas.getItemByIndex(i)
	
	'5-1.如果 Activity Assignment 具有 Criteria，則必須判讀 Criteria 是否回傳 OK
	If aa.getProperty("criteria") <> "" Then
		result = inn.applyMethod("EvaluateCriteria", "<ItemTypeID>" + item_type_id + "</ItemTypeID><ItemType>" + item_type + "</ItemType><ItemID>" + item_id + "</ItemID><CriteriaID>" + aa.getProperty("criteria") + "</CriteriaID>")
		
		If result.isError() Then
			err = inn.newError(result.getErrorString())
			Return err
		ElseIf CInt(result.getResult()) = 0 Then
			Continue For
		End If
	End If
	
	'5-2.取出所有 Assignments
	assignments = inn.newItem("Assignment", "get")
	assignments.setAttribute("select", "id, assignment_type, assignee, parameter, parameter2, is_required, for_all_members, voting_weight, escalate_to")
	assignments.setProperty("source_id", aa.getID())
	r_assignments = assignments.apply()
	num = r_assignments.getItemCount()
	
	If num = -1 Then
		err = inn.newError(r_assignments.getErrorString())
		Return err
	End If

	For j=0 To num-1
		assignment = r_assignments.getItemByIndex(j)
		ass_type = assignment.getProperty("assignment_type")
		
		If ass_type = "Identity" Then '5-3.Add an Identity To this Activity
			If Not assignment.node.selectSingleNode("//assignee") Is Nothing Then   '若 assignment_type = identity 時， assignee 必須有值
'				System.Diagnostics.Debugger.Break() 
				assignee_id = assignment.getProperty("assignee", "")
				
				'===============================================================================================================================
				'2010/11/02：修正 Assignee 如果填入系統預設的 Identity (Creator, Manager, Owner) 會出現比對不到
				'(1) 比對 assignee 是否是 Creator, Manager or Owner
				'(2) 轉換 assignee 成為正確的 Identity
				identity_type = inn.applyMethod("RecognizeBuildInIdentity", "<IdentityID>" + assignee_id + "</IdentityID>")
				
				If identity_type.isError() Then
					err = inn.newError(identity_type.getErrorString())
					Return err
				Else
					Select Case identity_type.getResult()
						Case "created_by_id"
							assignee_id = creator_id
						Case "managed_by_id"
							assignee_id = controlledItem.getProperty("managed_by_id", "")
						Case "owned_by_id"
							assignee_id = controlledItem.getProperty("owned_by_id", "")
					End Select
			
					If assignee_id = "" Then
						err = inn.newError("Fail to get this assignee. "  & identity_type.getResult() & ", "& assignment.getProperty("assignee", ""))
						Return err
					End If
				End If
				
				'===============================================================================================================================
				'2009/11/25: 檢查此 Identity 是否已經是 Assignment
				exist_flag = inn.applyMethod("CheckAssignmentIsExist", "<ActivityID>" + Me.getID() + "</ActivityID><IdentityID>" + assignee_id + "</IdentityID>")
				
				If exist_flag.isError() Then
					err = inn.newError(exist_flag.getErrorString())
					Return err
				Else
					If exist_flag.getResult() = "True" Then  '此 Identity 已經是 Assignment
						Continue For
					End If
				End If
				'===============================================================================================================================
				
				ass = Me.newItem("Activity Assignment","add")
				ass.setAttribute("leaveLocked","0")
				ass.setProperty("source_id", Me.getID())
				ass.setProperty("related_id", assignment.getProperty("assignee"))
				ass.setProperty("is_required", assignment.getProperty("is_required"))
				ass.setProperty("for_all_members", assignment.getProperty("for_all_members"))
				ass.setProperty("voting_weight", assignment.getProperty("voting_weight"))
				ass.setProperty("escalate_to", assignment.getProperty("escalate_to"))
				r_ass = ass.apply()
				
				If r_ass.isError() Then
					err = inn.newError(r_ass.getErrorString())
					Return err
				End If
			Else
				err = inn.newError("If assignment_type is Identity, then assignee must been filled.")
				Return err
			End If
		ElseIf ass_type = "Manager" Then ' 5-4.Add a Manager to this Activity
			'取出階層
			If (Not assignment.getProperty("parameter") Is Nothing) Then
				If IsNumeric(assignment.getProperty("parameter")) = False Then	
					err = inn.newError("If assignment_type is Manager, then parameter must be a integer.")
					Return err
				Else
					level = CInt(assignment.getProperty("parameter"))
				End If
			Else
				level = 1
			End If
	
			level_mgr_id = creator_id
	
			'取出 Manager	
			While level > 0 
				org = inn.newItem("Organization", "get")
				oi = inn.newItem("Organization Identity", "get")
				oi.setProperty("related_id", level_mgr_id)
				org.addRelationship(oi)
				r_org = org.apply()
				
				If r_org.isError() Then
					identity = inn.getItemByID("Identity", level_mgr_id)
					err = inn.newError("Can not find any organization that the identity belong to. Identity=[" & identity.getProperty("name") & "]")
					Return err
				End If
			
				If Not r_org.node.selectSingleNode("//manager")  Is Nothing Then
					level = level -1
					
					If level > 0 Then
						level_mgr_id = r_org.getProperty("manager")    
					Else
						manager_id = r_org.getProperty("manager")
					End If
				Else
					err = inn.newError(r_org.getProperty("name") & " : does not define the manager.")
					Return err
				End If
			
				org = Nothing
				r_org = Nothing
			End While
	
			'將 Manager 加入 Workflow
			If manager_id <> "" Then	
				'===============================================================================================================================
				'2009/11/25: 檢查此 Identity 是否已經是 Assignment
				exist_flag = inn.applyMethod("CheckAssignmentIsExist", "<ActivityID>" + Me.getID() + "</ActivityID><IdentityID>" + manager_id + "</IdentityID>")
				
				If exist_flag.isError() Then
					err = inn.newError(exist_flag.getErrorString())
					Return err
				Else
					If exist_flag.getResult() = "True" Then  '此 Identity 已經是 Assignment
						Continue For
					End If
				End If
				'===============================================================================================================================
				
				ass = Me.newItem("Activity Assignment","add")
				ass.setAttribute("leaveLocked","0")
				ass.setProperty("source_id", Me.getID())
				ass.setProperty("related_id", manager_id)
				ass.setProperty("is_required", assignment.getProperty("is_required"))
				ass.setProperty("for_all_members", assignment.getProperty("for_all_members"))
				ass.setProperty("voting_weight", assignment.getProperty("voting_weight"))
				ass.setProperty("escalate_to", assignment.getProperty("escalate_to"))
				r_ass = ass.apply()
				
				If r_ass.isError() Then
					err = inn.newError(r_ass.getErrorString())
					Return err
				End If
			Else
				err = inn.newError(r_org.getProperty("name") & " : does not define the manager.")
				Return err
			End If
	
		ElseIf ass_type = "Field" Then
			If Not assignment.getProperty("parameter") Is Nothing Then
				field_name = assignment.getProperty("parameter")
				
				If Not controlledItem.getProperty(field_name) Is Nothing Then
					manager_id = controlledItem.getProperty(field_name)
					
					If manager_id Is Nothing Then
						err = inn.newError( "Can not get the value  by parameter1.")
						Return err
					End If
						
					'===============================================================================================================================
					'2009/11/25: 檢查此 Identity 是否已經是 Assignment
					exist_flag = inn.applyMethod("CheckAssignmentIsExist", "<ActivityID>" + Me.getID() + "</ActivityID><IdentityID>" + manager_id + "</IdentityID>")
					
					If exist_flag.isError() Then
						err = inn.newError(exist_flag.getErrorString())
						Return err
					Else
						If exist_flag.getResult() = "True" Then  '此 Identity 已經是 Assignment
							Continue For
						End If
					End If
					'===============================================================================================================================
					
					ass = Me.newItem("Activity Assignment","add")
					ass.setAttribute("leaveLocked","0")
					ass.setProperty("source_id", Me.getID())
					ass.setProperty("related_id", manager_id)
					ass.setProperty("is_required", assignment.getProperty("is_required"))
					ass.setProperty("for_all_members", assignment.getProperty("for_all_members"))
					ass.setProperty("voting_weight", assignment.getProperty("voting_weight"))
					ass.setProperty("escalate_to", assignment.getProperty("escalate_to"))
					r_ass = ass.apply()
					
					If r_ass.isError() Then
						err = inn.newError(r_ass.getErrorString())
						Return err
					End If
				End If
			Else
				err = inn.newError("If assignment_type is Field, then parameter must be filled.")
				Return err
			End If
		ElseIf ass_type = "Item Field" Then
			If (assignment.getProperty("parameter") isnot Nothing) AndAlso (assignment.getProperty("parameter2") isnot Nothing) Then
				item_field_name  = assignment.getProperty("parameter")
				field_name = assignment.getProperty("parameter2")
				
				If Not controlledItem.getProperty(item_field_name) Is Nothing Then
					'透過 Controlled Item 的 ItemTypeID 及 屬性名稱(item_field_name) 取出 Data Source，此 Data Source 即是 ItemType Name
					property_item = inn.newItem("Property", "get")
					property_item.setAttribute("select", "id, name, data_type, data_source")
					property_item.setProperty("source_id", item_type_id)
					property_item.setProperty("name", item_field_name)
					r_property_item = property_item.apply()
					
					If r_property_item.isError() Then
						err = inn.newError( item_type + " ItemType does not contain this " + item_field_name + " property.")
						Return err
					End If
					
					itm_data_type = r_property_item.getProperty("data_type")
					
					If itm_data_type <> "item" Then
						err = inn.newError("The data type of " + item_field_name + " property must be Item.")
						Return err
					End If
					
					itm_itemtype_id = r_property_item.getProperty("data_source")
					
					it = inn.newItem("ItemType", "get")
					it.setAttribute("select", "id, name")
					it.setID(itm_itemtype_id)
					r_it = it.apply()
					
					If r_it.isError() Then
						err = inn.newError("Can not get the name of ItemType.")
						Return err
					End If
					
					itm_itemtype_name = r_it.getProperty("name")
					itm_id = controlledItem.getProperty(item_field_name)
					
					itm = inn.newItem(itm_itemtype_name, "get")
					itm.setAttribute("select", "id, " + field_name)
					itm.setID(itm_id)
					r_itm = itm.apply()
					
					If r_itm.isError() Then
						err = inn.newError("Can not find this item by parameter1 and parameter2.")
						Return err
					End If
					
					manager_id = r_itm.getProperty(field_name)
					
					If manager_id Is Nothing Then
						err = inn.newError("Can not get the value  by parameter1 and parameter2")
						Return err
					End If
					
					'===============================================================================================================================
					'2009/11/25: 檢查此 Identity 是否已經是 Assignment
					exist_flag = inn.applyMethod("CheckAssignmentIsExist", "<ActivityID>" + Me.getID() + "</ActivityID><IdentityID>" + manager_id + "</IdentityID>")
					
					If exist_flag.isError() Then
						err = inn.newError(exist_flag.getErrorString())
						Return err
					Else
						If exist_flag.getResult() = "True" Then  '此 Identity 已經是 Assignment
							Continue For
						End If
					End If
					'===============================================================================================================================
								
					ass = Me.newItem("Activity Assignment","add")
					ass.setAttribute("leaveLocked","0")
					ass.setProperty("source_id", Me.getID())
					ass.setProperty("related_id", manager_id)
					ass.setProperty("is_required", assignment.getProperty("is_required"))
					ass.setProperty("for_all_members", assignment.getProperty("for_all_members"))
					ass.setProperty("voting_weight", assignment.getProperty("voting_weight"))
					ass.setProperty("escalate_to", assignment.getProperty("escalate_to"))
					r_ass = ass.apply()
					
					If r_ass.isError() Then
						err = inn.newError(r_ass.getErrorString())
						Return err
					End If
				End If
			Else
				err = inn.newError("If assignment_type is Item Field, then parameter and parameter2 must be filled.")
				Return err
			End If
		End If
	
		' Change the Activity text (just so a change is visible in the InBasket)
		act = Me.newItem(Me.GetType(),"edit")
		act.setID(Me.getID())
		act.setProperty("message",  "Assignments Added")
		r_act = act.apply()
		
		If r_act.isError() Then
			err = inn.newError(r_act.getErrorString())
			Return err
		End If
	Next
Next 
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='AddAssignmentsToWFWithCriteria32' and [Method].is_current='1'">
<config_id>925E3BED007C488ABE7C18608740F57A</config_id>
<name>AddAssignmentsToWFWithCriteria32</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
