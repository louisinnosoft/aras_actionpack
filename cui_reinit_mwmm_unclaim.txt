var disabled = true;
if (inArgs.isReinit) {
	if (!inArgs.eventState.InBasketEventState) {
		var states = aras.evalMethod('cui_reinit_inbasket_calc_states', '', {eventType: inArgs.eventType});
		inArgs.eventState.InBasketEventState = states.InBasketEventState;
	}

	if (inArgs.eventState.InBasketEventState) {
		var inBasketEventState = inArgs.eventState.InBasketEventState;
		var isFunctionDisabledBasedOnItemTypes = false;
		for (var i = 0; i < inBasketEventState.itemTypesNames.length; i++) {
			if (isFunctionDisabled(inBasketEventState.itemTypesNames[i], 'Unlock')) {
				isFunctionDisabledBasedOnItemTypes = true;
				break;
			}
		}
		disabled = !(inBasketEventState.unlockFlg && !isFunctionDisabledBasedOnItemTypes);
	}
}

return {'cui_disabled': disabled};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_reinit_mwmm_unclaim' and [Method].is_current='1'">
<config_id>F1F83BD2C452483DB1C77FF927CC9792</config_id>
<name>cui_reinit_mwmm_unclaim</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
