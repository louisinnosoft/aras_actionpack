var topWindow = aras.getMostTopWindowWithAras(window);
var menuFrame = topWindow.menu;
if (menuFrame && menuFrame.onShowControlsApiReferenceJavaScriptCommand) {
	menuFrame.onShowControlsApiReferenceJavaScriptCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwmm_controlsApiJS' and [Method].is_current='1'">
<config_id>47163192C019450CA9116CEF59C43389</config_id>
<name>cui_default_mwmm_controlsApiJS</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
