/*version:1*/
var dataType = getRelationshipProperty(relationshipID, 'data_type');
if (dataType != 'item') {
	return false;
}
return true;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='OnEditStart for item_behavior' and [Method].is_current='1'">
<config_id>3D1911B178AC466C99393A39D0E80592</config_id>
<name>OnEditStart for item_behavior</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
