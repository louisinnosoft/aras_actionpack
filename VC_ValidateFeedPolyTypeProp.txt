string reference = this.getProperty("reference");
string polysourceTypeName = this.getProperty("polysource_type_name");
string ddId = this.getProperty("source_id");
if (!string.IsNullOrEmpty(reference))
{
	Item ddItem = this.newItem("DiscussionDefinition", "get");
	ddItem.setID(ddId);
	ddItem.setAttribute("select", "item_type_name");
	ddItem = ddItem.apply();
	if (ddItem.isError())
	{
		this.getInnovator().newError(ddItem.getErrorString());
	}

	string itemTypeName = ddItem.getProperty("item_type_name");
	if (string.IsNullOrEmpty(itemTypeName))
	{
		return this.getInnovator().newError(CCO.ErrorLookup.Lookup("SSVC_UnableGetItemTypeName"));
	}

	Item validateItem = this.newItem("Feed", "VC_ValidatePolysourceTypeProp");
	validateItem.setID(this.getID());
	validateItem.setAttribute("discussType", "DiscussionDefinition");
	validateItem.setProperty("item_type_name", itemTypeName);
	validateItem.setProperty("reference", reference);
	validateItem.setProperty("polysource_type_name", polysourceTypeName);
	validateItem = validateItem.apply();
	if (validateItem.isError())
	{
		return validateItem;
	}
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VC_ValidateFeedPolyTypeProp' and [Method].is_current='1'">
<config_id>BB338B14833D4B53A2718EF02474A75A</config_id>
<name>VC_ValidateFeedPolyTypeProp</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
