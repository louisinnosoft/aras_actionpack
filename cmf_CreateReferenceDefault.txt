var topWindow = aras.getMostTopWindowWithAras(window);
var asyncResult = topWindow.ModulesManager.createAsyncResult();
var boundItem = aras.newIOMItem(inArgs.referencedTypeName, 'add');
var uiUtils = new CMF.UI.Utils();

uiUtils.showEditorTearOff(boundItem, function() {
	asyncResult.resolve(boundItem.getID());
});

return asyncResult;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_CreateReferenceDefault' and [Method].is_current='1'">
<config_id>9428EBF79A4642179FF83FF8A6B39DB2</config_id>
<name>cmf_CreateReferenceDefault</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
