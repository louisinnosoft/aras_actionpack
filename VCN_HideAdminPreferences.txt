// By the SSVC Notification specification "Administrative Preferences" available only for "World" identity

var identityName = aras.getItemPropertyAttribute(document.item, 'source_id', 'keyed_name');
if (!identityName || identityName !== 'World') {
	var collection = document.getElementsByName('administrative_preferences');
	if (collection.length > 0) {
		var groupBox  =  collection[0];
		if (groupBox.style) {
			groupBox.style.display = 'none';
		}
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VCN_HideAdminPreferences' and [Method].is_current='1'">
<config_id>019449B5E13240DC958DFF7746213BD3</config_id>
<name>VCN_HideAdminPreferences</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
