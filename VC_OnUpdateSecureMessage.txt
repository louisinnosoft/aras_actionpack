	HashSet<string> allowedProperties = new HashSet<string>
	{
		"disabled_by_id",
		"disabled_on"
	};
	this.callContext = CCO;
	for (int i = this.node.ChildNodes.Count - 1; i >= 0; i--)
	{
		XmlNode child = this.node.ChildNodes[i];
		if (child.NodeType != XmlNodeType.Whitespace && !allowedProperties.Contains(child.Name))
		{
			child.ParentNode.RemoveChild(child);
		}
		else
		{
			CheckForDisableAction(child);
		}
	}

	if (!isDisableMessage)
	{
		throw new Exception("Messages could be only disabled.");
	}

	if (disabledByIdItm == null && String.IsNullOrEmpty(disabledById))
	{
		disabledById = CCO.Variables.GetUserID();
		this.setProperty("disabled_by_id", disabledById);
	}
	else
	{
		if (String.IsNullOrEmpty(disabledOn))
		{
			I18NSessionContext sessionContext = this.getInnovator().getI18NSessionContext();
			disabledOn = sessionContext.ConvertUtcDateTimeToNeutral(DateTime.UtcNow.ToString(CultureInfo.InvariantCulture), String.Empty);
			this.setProperty("disabled_on", disabledOn);
		}
	}

	string[] idList = null;
	try
	{
		idList = CheckSecureMessages(this);
	}
	catch (Exception ex)
	{
		return this.getInnovator().newError(ex.Message);
	}

	this.removeAttribute("id");
	this.removeAttribute("where");
	this.setAttribute("idlist", String.Join(",", idList));
	this.setType("StoredSecureMessage");
	this.setAction("edit");
	SetPermissionsToRelationships(this, idList);

	Aras.Server.Security.Identity SMRWIdentity = null;
	bool permsWasSet = false;
	try
	{
		SMRWIdentity = Aras.Server.Security.Identity.GetByName("SecureMessageReaderWriter");
		permsWasSet = Aras.Server.Security.Permissions.GrantIdentity(SMRWIdentity);
		Item result = this.apply();
		return result;
	}
	finally
	{
		if (permsWasSet)
		{
			Aras.Server.Security.Permissions.RevokeIdentity(SMRWIdentity);
		}
	}
}

private void SetPermissionsToRelationships(Item item, string[] sourceIds)
{
	const string permissionId = "4D61B5EC0D2448D08B8C4A596F48B0F1";
	
	string[] types = new string[] { "SecureMessageAudio", "SecureMessageMarkup", "SecureMessageVideo" };
	for (int i = 0; i < types.Length; i++)
	{
		for (int j = 0 ; j < sourceIds.Length; j++)
		{
			Item relship= this.newItem(types[i], "edit");
			relship.setProperty("permission_id", permissionId);
			relship.setAttribute("where", String.Format(CultureInfo.InvariantCulture, "source_id = '{0}'", sourceIds[j]));
			item.addRelationship(relship);
		}
	}
}

private string disabledOn;
private string disabledById;
private Item disabledByIdItm;

private bool isDisableMessage;
private Aras.Server.Core.CallContext callContext;

private void CheckForDisableAction(XmlNode node)
{
	if (string.Equals(node.Name, "disabled_by_id"))
	{
		disabledByIdItm = this.getPropertyItem("disabled_by_id");
		disabledById = this.getProperty("disabled_by_id");
	}
	if (string.Equals(node.Name, "disabled_on"))
	{
		disabledOn = this.getProperty("disabled_on");
	}
	if (disabledByIdItm != null || !String.IsNullOrEmpty(disabledById) || !String.IsNullOrEmpty(disabledOn))
	{
		isDisableMessage = true;
	}
}

private string[] CheckSecureMessages(Item originItem)
{
	List<string> idsToUpdate = new List<string>();
	Item query = originItem.newItem("SecureMessage", "get");
	if (!String.IsNullOrEmpty(originItem.getAttribute("id")))
	{
		query.setAttribute("id", originItem.getAttribute("id"));
	}
	if (!String.IsNullOrEmpty(originItem.getAttribute("idlist")))
	{
		query.setAttribute("idlist", originItem.getAttribute("idlist"));
	}
	if (!String.IsNullOrEmpty(originItem.getAttribute("where")))
	{
		query.setAttribute("where", originItem.getAttribute("where"));
	}
	query.setAttribute("select", "id, created_by_id, created_on, classification, item_id, item_config_id, item_type, item_type_name, disabled_by_id");

	Item result = query.apply();
	if (result.isError())
	{
		throw new Exception(result.getErrorString());
	}
	for (int i = 0; i < result.getItemCount(); i++)
	{
		Item resultItem = result.getItemByIndex(i);
		if (resultItem.getProperty("classification") == "History")
		{
			throw new Exception(callContext.ErrorLookup.Lookup("SSVC_HistoryMessagesCannotBeDisabled"));
		}
		CheckSecureMessagePermissions(resultItem);
		idsToUpdate.Add(resultItem.getID());
	}
	return idsToUpdate.ToArray();
}

private void CheckSecureMessagePermissions(Item sm)
{
	if (String.IsNullOrEmpty(sm.getProperty("disabled_by_id")))
	{
		if (!callContext.Permissions.UserHasRootOrAdminIdentity())
		{
			string userId = callContext.Variables.GetUserID();
			string createdById = sm.getProperty("created_by_id");
			if (String.IsNullOrEmpty(createdById) || !string.Equals(createdById, userId))
			{
				throw new Exception(callContext.ErrorLookup.Lookup("SSVC_MassageCanBeDisabledByAdminOrCreator"));
			}
		}
	}
	else
	{
		throw new Exception(callContext.ErrorLookup.Lookup("SSVC_MessageIsDisabled"));
	}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VC_OnUpdateSecureMessage' and [Method].is_current='1'">
<config_id>86A11F521FD344C38192876269D008D9</config_id>
<name>VC_OnUpdateSecureMessage</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
