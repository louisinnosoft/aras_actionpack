var self = this;

if (!window.myClientUpgrade)
{
  function startMainWhenReady()
  {
    var srcElement = window.event.srcElement;
    if (srcElement.readyState == 4)
    {
      srcElement.detachEvent("onreadystatechange", startMainWhenReady);
      window.myClientUpgrade = srcElement.object;
      main.call(self);
    }
  }

  var msHelperClassId = top.aras.getBaseURL() + "/cbin/clientupgrade.dll#Utils.ClientUpgrade";
  top.aras.uiAddConfigLink2Doc4Assembly(document, "ClientUpgrade");
  var tagObject = document.createElement("<OBJECT id='clientUpgrade' classId='" + msHelperClassId + "'></OBJECT>");
  tagObject.attachEvent("onreadystatechange", startMainWhenReady);
  
  var headObject = document.getElementsByTagName("HEAD")[0];
  headObject.appendChild(tagObject);
}
else
{
  main.call(self);
}

// main function
function main()
{

var innov=top.aras.newIOMInnovator();
var base_url=top.aras.getBaseURL();
base_url=base_url.substr(0,base_url.indexOf("/Client"));
var db=top.aras.getDatabase();
var user="root";
var pw="innovator";
var main_wnd=top.aras.getMainWindow();
var vault=main_wnd.vault;
var mf_text="";
var folder=vault.selectFolder();

var cu=document.clientUpgrade;
cu.login(base_url,db,user,pw,folder);

var rows=this.getRelationships("PE");

for (var i=0;i<rows.getItemCount();i++)
{
	var this_row=rows.getItemByIndex(i);
		var this_id=this_row.getID();
		var this_pd=this_row.getProperty("pd");
		var this_pd_folder=this_pd.replace("com.aras.innovator.solution.","");
		var this_type=this_row.getProperty("pg");
		if (this_type==="RelationshipType (ItemType)") {this_type="RelationshipType";}
    var this_kn=this_row.getProperty("kn");
		var this_folder=folder+"\\"+this_pd_folder+"\\Import\\";
		cu.setfolder(this_folder);
		cu.packageItem(this_id, this_kn, this_type, this_pd );
		if (mf_text.indexOf(this_pd)<0)
		{ 
		   mf_text+=" <package name='"+this_pd+"' path='"+this_pd_folder+"\\Import\\' />\r\n";
		}
}
mf_text="<imports>\r\n"+mf_text+"</imports>\r\n";
ok = vault.WriteText(folder+"\\imports.mf",mf_text);
if (!ok) { top.aras.AlertError("Error writing file manifest file");}

top.aras.AlertSuccess("Customizations exported successfully");
}
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cust_rep_export' and [Method].is_current='1'">
<config_id>AD2F1293897442EA992871662D609D02</config_id>
<name>cust_rep_export</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
