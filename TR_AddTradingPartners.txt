Aras.Server.FileExchange.TrubiquityCoreConnector.TruFileExchangeService ts = 
	(Aras.Server.FileExchange.TrubiquityCoreConnector.TruFileExchangeService)Aras.Server.FileExchange.FileExchangeServiceFactory.CreateExchangeService("Aras.TrubiquityTRUAras");

XmlDocument doc = new XmlDocument();
string data = this.getPropertyAttribute("body", "data");

if (string.IsNullOrEmpty(data))
{
	throw new ArgumentNullException("data");
}
doc.LoadXml(data);

XmlNodeList rsNodes = doc.SelectNodes("/recipients/recipient");

var partners = new List<Aras.Server.FileExchange.TrubiquityCoreConnector.RecipientInfo>();
for (int i = 0; i < rsNodes.Count; i++)
{
	partners.Add(new Aras.Server.FileExchange.TrubiquityCoreConnector.RecipientInfo(Convert.ToInt32(rsNodes[i].InnerText)));
}

ts.User.AddTradingPartners(partners.ToArray());

return this.getInnovator().newResult("OK"); 
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='TR_AddTradingPartners' and [Method].is_current='1'">
<config_id>10C30491433743C5B79549C5DB2D781F</config_id>
<name>TR_AddTradingPartners</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
