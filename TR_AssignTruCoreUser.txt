var isClick = (this.tagName || "").toLowerCase() == "input";
if (isClick && !document.isEditMode) {
	return;
}

if (!top.aras.isAdminUser()) {
	top.aras.AlertError(top.aras.getResource("", "tr.assign_not_admin_error"));
	return;
}

var params = {};
params.aras = top.aras;
params.isClick = isClick;
params.title = isClick ? top.aras.getResource("", "tr.assign_truaras_users_title") : top.aras.getResource("", "tr.manage_truaras_users_title");
params.requestedPropertiesLabels = [
	top.aras.getResource("", "tr.user_name"),
	top.aras.getResource("", "tr.first_name"),
	top.aras.getResource("", "tr.last_name"),
	top.aras.getResource("", "tr.middle_name"),
	top.aras.getResource("", "tr.title_name"),
	top.aras.getResource("", "tr.phone"),
	top.aras.getResource("", "tr.fax")
];
params.requestedPropertiesColumnsWidth = ["18", "18", "18", "18", "4", "12", "12"];
params.requestedPropertiesColumnsAligns = ["left", "left", "left", "left", "left", "left", "left"];

var form = top.aras.getItemByName("Form", "TR_GetTruCoreLocationUsers", 0);
var width = top.aras.getItemProperty(form, "width") || 800;
var height = top.aras.getItemProperty(form, "height") || 600;
params.formId = top.aras.getItemProperty(form, "id");

var options = {
	dialogWidth: width,
	dialogHeight: height
};

var res = top.aras.modalDialogHelper.show("DefaultModal", window, params, options, "ShowFormAsADialog.html");
if (res) {
	if (!isClick) {
		top.aras.AlertSuccess(top.aras.getResource("", "tr.association_created")); 
	}
	else {
		window.handleItemChange("login_name", res);
	}
}
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='TR_AssignTruCoreUser' and [Method].is_current='1'">
<config_id>4FCE97130D844520836BB4F30A7846F5</config_id>
<name>TR_AssignTruCoreUser</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
