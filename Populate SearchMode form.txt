var itm = document.item;
var isNew = ((itm.getAttribute('action') == 'add') || (itm.getAttribute('action') == 'create'));

if (!itm || !document.isEditMode || !isNew) {
	return;
}

var searchHandler = itm.selectSingleNode('search_handler');
if (!searchHandler) {
	searchHandler =
	'<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\'\"http://www.w3.org/TR/html4/strict.dtd\">\n' +
	'<html>\n' +
		'<head>\n' +
		'<title><\/title>\n' +
		'<\/head>\n' +
		'<body>\n' +
			'<script type=\"text\/javascript\">\n' +
				'eval(parent.aras.getFileText(parent.aras.getBaseURL() + \"\/javascript\/include.aspx?classes=XmlDocument\"));\n' +
				'eval(parent.aras.getFileText(parent.aras.getBaseURL() + \"\/javascript\/search_mode.js\"));\n' +
				'NewSearchMode.prototype = new SearchMode;\n' +
				'function NewSearchMode()\n' +
				'{\n' +
				'}\n' +
				'searchMode = new NewSearchMode();\n' +
			'<\/script>\n' +
		'<\/body>\n' +
	'<\/html>';

	aras.setItemProperty(itm, 'search_handler', searchHandler);
	handleItemChange('search_handler', searchHandler);
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='Populate SearchMode form' and [Method].is_current='1'">
<config_id>7572CEBDDF29418D86981FFBDB3E7554</config_id>
<name>Populate SearchMode form</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
