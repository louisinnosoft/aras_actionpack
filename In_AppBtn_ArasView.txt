/*
目的:APPAction:回ARAS檢視的按鈕
用法:item.apply
須回傳:
app action
(若回傳 null 則代表不顯示按鈕)

位置:App Action的method
*/
string strMethodName = "In_AppBtn_ArasView";
//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
Innosoft.app _InnoApp = new Innosoft.app(inn);
string aml = "";
string sql = "";
string strError = "";

_InnH.AddLog(strMethodName,"MethodSteps");
Item itmAction = null;
//只有檢視模式才需要處理
if(this.getProperty("sort","")=="in_web_add" || this.getProperty("sort","")=="in_web_edit")
	return  itmAction;

string protocol = "http";
string host  = HttpContext.Current.Request.Url.Host;
int port = HttpContext.Current.Request.Url.Port;
string vDir = "";
int count  = HttpContext.Current.Request.Url.Segments.Length;
for(int index=1;index<=count - 3;index++)
{
	//'Ignoring "Server/InnovatorServer.aspx"
	vDir +=  HttpContext.Current.Request.Url.Segments[index];
}	
	
if(port!=80)
{
	host +=  ":" + port;
}	

if(HttpContext.Current.Request.IsSecureConnection!=false)
{
	protocol = "https";
}

string strhref=protocol + ":////" + host + "//" + vDir + "default.aspx?StartItem=" + this.getType() + ":" + this.getID();

itmAction = inn.newItem();
itmAction.setType("action");
itmAction.setProperty("href",strhref);
itmAction.setProperty("btn_type","report");		
itmAction.setProperty("label","Aras");
itmAction.setProperty("class","");
itmAction.setProperty("data_toggle","");
itmAction.setProperty("data_target","");
itmAction.setProperty("attributes","target='_blank'");

return itmAction;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_AppBtn_ArasView' and [Method].is_current='1'">
<config_id>9687DC8DFF4E49D59ACB00A235174B52</config_id>
<name>In_AppBtn_ArasView</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
