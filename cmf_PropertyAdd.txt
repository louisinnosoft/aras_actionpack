Item documentTypeQuery = this.newItem("cmf_ElementType", "get");
documentTypeQuery.setID(this.getProperty("source_id"));
documentTypeQuery.setAttribute("select", "source_id");
documentTypeQuery = documentTypeQuery.apply();
string documentTypeId = documentTypeQuery.getProperty("source_id");

if (!RequestState.Contains(documentTypeId))
{
    return this.getInnovator().newError("It's impossible to save certain Element Type or Property Type. Instead save entire Content Type item.");
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_PropertyAdd' and [Method].is_current='1'">
<config_id>863B2A7CC00B450AA49C34C72BA7A8DE</config_id>
<name>cmf_PropertyAdd</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
