string sourceId = this.getProperty("source_id");
if (String.IsNullOrEmpty(sourceId))
{
	throw new Exception(CCO.ErrorLookup.Lookup("SSVC_DeleteFMGSourceIdNotSet"));
}
string type = this.getProperty("group_type");
if (String.IsNullOrEmpty(type))
{
	throw new Exception(CCO.ErrorLookup.Lookup("SSVC_DeleteFMGGroupTypeNotSet"));
}
string criteriaId = getProperty("user_criteria_id");
if (String.IsNullOrEmpty(criteriaId))
{
	throw new Exception(CCO.ErrorLookup.Lookup("SSVC_DeleteFMGUserCriteriaIdNotSet"));
}

Item fmg = this.newItem("ForumMessageGroup", "delete");
fmg.setAttribute("where", GetWhereCriteria(sourceId, type, criteriaId));
Item result = fmg.apply();
if (result.isError())
{
	throw new Exception(String.Format(CultureInfo.InvariantCulture, CCO.ErrorLookup.Lookup("SSVC_DeleteFMG"), result.getErrorString()));
}
return result;
}

private string GetWhereCriteria(string sourceId, string type, string criteriaId)
{
	const string tableName = "FORUMMESSAGEGROUP";
	return String.Format(CultureInfo.InvariantCulture, "[{3}].source_id='{0}' and [{3}].group_type='{1}' and [{3}].user_criteria_id='{2}'", sourceId, type, criteriaId, tableName);

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VC_DeleteForumMessageGroup' and [Method].is_current='1'">
<config_id>90C3EF8F4025456FB2B000A0344F012B</config_id>
<name>VC_DeleteForumMessageGroup</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
