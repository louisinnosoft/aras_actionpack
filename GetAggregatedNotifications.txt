// return Message Notifications for user

string userId = this.getProperty("user_id");
string expirationDate = this.getProperty("expiration_date");

// this aml was copy from old realization of Notification Messages

string aml = @"<Item type='Message' action='get'>
          <OR>
            <Relationships>
              <Item type='Message Acknowledgement' action='get' select='id,related_id'>
                <related_id>
                  <Item type='User' action='get' select='id,keyed_name'>
                    <NOT>
                      <id condition='eq'>$CurrentUserID</id>
                    </NOT>
                  </Item>
                </related_id>

                <NOT>
                  <source_id>
                    <Item type='Message' action='get'>
                      <Relationships>
                        <Item type='Message Acknowledgement' action='get' select='id,related_id'>
                          <related_id>$CurrentUserID</related_id>
                        </Item>
                      </Relationships>
                    </Item>
                  </source_id>
                </NOT>
              </Item>
            </Relationships>

            <NOT>
              <Relationships>
                <Item type='Message Acknowledgement' action='get'>
                  <source_id condition='is not null'></source_id>
                </Item>
              </Relationships>
            </NOT>
          </OR>
          <OR>
            <expiration_date condition='gt'>$DateTime</expiration_date>
            <expiration_date condition='is null'></expiration_date>
          </OR>
          <OR>
            <target>
              <Item type='Identity' action='get'>
                <Relationships>
                  <Item type='Member' action='get'>
                    <related_id>
                      <Item type='Identity' action='get'>
                        <id condition='in'>
                          SELECT related_id FROM [Alias] WHERE
                          source_id = '$CurrentUserID'
                        </id>
                      </Item>
                    </related_id>
                  </Item>
                </Relationships>
              </Item>
            </target>
            <target>
              <Item type='Identity' action='get' >
                <id condition='in'>
                  SELECT related_id FROM [Alias] WHERE
                  source_id = '$CurrentUserID'
                </id>
              </Item>
            </target>
            <target>
              <Item type='Identity' action='get'>
                <keyed_name condition='eq'>World</keyed_name>
              </Item>
            </target>
          </OR>
        </Item>";

aml = aml.Replace("$CurrentUserID", userId).Replace("$DateTime", expirationDate);
var res = this.newItem();
res.loadAML(aml);
return res.apply();
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='GetAggregatedNotifications' and [Method].is_current='1'">
<config_id>D733769DC0BE42A5AB57D0B327F069B7</config_id>
<name>GetAggregatedNotifications</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
