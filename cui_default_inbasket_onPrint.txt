var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
if (workerFrame && workerFrame.onPrintCommand) {
	workerFrame.onPrintCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_inbasket_onPrint' and [Method].is_current='1'">
<config_id>2425DAB236654398AD985E38BE07D2B4</config_id>
<name>cui_default_inbasket_onPrint</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
