'Create Date: 2009/02/13, Creator: Scott

'System.Diagnostics.Debugger.Break() 

Dim inn As Innovator = Me.getInnovator()
Dim err As Item
Dim wfm_name As String       'Workflow Map name
Dim at_name As String           'Activity Template Name
Dim item_type As String         'The item type of controlled item
Dim item_id As String            'The id of controlled item
Dim item_type_id As String    'The id of item type of controlled item
Dim identity As Item                'Creator's user identity
Dim creator_id As String         'Creator ID

'******************************************************************************************************************
'1.找出 Workflow Map & Activity Template 名稱
Dim wfp As Item = inn.newItem("Workflow Process", "get")
Dim wfpa As Item = inn.newItem("Workflow Process Activity", "get")
wfpa.setProperty("related_id", Me.getID())
wfp.addRelationship(wfpa)
Dim r_wfp As Item = wfp.apply()

If r_wfp.isError() Then
	err = inn.newError(r_wfp.getErrorString())
	Return err
End If

Dim wfm As Item = inn.newItem("Workflow Map", "get")
wfm.setID(r_wfp.getProperty("copied_from_string"))
Dim r_wfm As Item = wfm.apply()

If r_wfm.isError() Then
	err = inn.newError(r_wfm.getErrorString())
	Return err
End If

wfm_name = r_wfm.getProperty("name")
at_name = Me.getProperty("name")

'******************************************************************************************************************
'2-1.取出 Controlled Item (表單)，並記錄其 ItemType
Dim controlledItem As Item
Dim ca, r_ca As Item

controlledItem = Me.newItem(Me.GetType(),"Get My Controlled Item")
controlledItem.setID(Me.getID())
controlledItem = controlledItem.apply()

If controlledItem.isError() Then
	err = inn.newError(controlledItem.getErrorString())
	Return err
End If

item_type = controlledItem.GetType()
item_id = controlledItem.getID()

'2-2.取出表單的 Creator
ca = inn.newItem("Alias", "get")
ca.setAttribute("select", "id, related_id")
ca.setProperty("source_id", controlledItem.getProperty("created_by_id")) 
r_ca = ca.apply()

If r_ca.isError() Then
	err = inn.newError(r_ca.getErrorString())
	Return err
End If

identity = r_ca.getRelatedItem().getItemByIndex(0)
creator_id = identity.getID()

'2-3.取得 The id of item type of controlled item
Dim it As Item = inn.getItemByKeyedName("ItemType", item_type)

If it.isError() Then
	err = inn.newError(it.getErrorString())
	Return err
End If

item_type_id = it.getID()

'******************************************************************************************************************
'3.取出 Assignment Matrix 名稱
Dim var As Item = inn.newItem("Variable", "get")
var.setProperty("name", "Current Assignment Matrix")
Dim r_var As Item = var.apply()

If r_var.isError() Then
	err = inn.newError(r_var.getErrorString())
	Return err
End If

Dim am_name As String = r_var.getProperty("value")

'******************************************************************************************************************
'4.取出符合條件的 Assignment Activity
Dim am As Item = inn.newItem("Assignment Matrix", "get")
am.setProperty("name", am_name)
Dim r_am As Item = am.apply()

If r_am.isError() Then
	err = inn.newError(r_am.getErrorString())
	Return err
End If

Dim aas As Item = inn.newItem("Assignment Activity", "get")
aas.setProperty("source_id", r_am.getID())
aas.setProperty("workflow_map", wfm_name)
aas.setProperty("activity_template", at_name)

Dim or_item1 As Item = aas.newOR()
Dim criteria As Item = inn.newItem("Criteria", "get")
criteria.setProperty("item_type", item_type_id)
or_item1.setPropertyItem("criteria", criteria)

Dim or_item2 As Item = or_item1.newOR()
or_item2.setProperty("criteria", "")
or_item2.setPropertyCondition("criteria", "is null")

Dim r_aas As Item = aas.apply()
Dim count As Integer = r_aas.getItemCount()

If count = -1 Then
	err = inn.newError(r_aas.getErrorString())
	Return err
End If

'******************************************************************************************************************
'5.Assign all assignments
Dim i, j, num As Integer
Dim aa As Item    'Assignment Activity
Dim assignments, r_assignments, assignment, r_assignment As Item
Dim ass_type As String  'Assignment Type：Manager、Identity、Field、Previous Activity Manager
Dim level As Integer
Dim ass, r_ass As Item    'Activity Assignment
Dim act, r_act As Item
Dim org, r_org, oi As Item
Dim result As item
Dim manager_id As String
Dim field_name As String


For i=0 To count-1
	aa = r_aas.getItemByIndex(i)
	
	'5-1.如果 Activity Assignment 具有 Criteria，則必須判讀 Criteria 是否回傳 OK
	If aa.getProperty("criteria") <> "" Then
		result = inn.applyMethod("EvaluateCriteria", "<ItemTypeID>" + item_type_id + "</ItemTypeID><ItemType>" + item_type + "</ItemType><ItemID>" + item_id + "</ItemID><CriteriaID>" + aa.getProperty("criteria") + "</CriteriaID>")
		
		If result.isError() Then
			err = inn.newError(result.getErrorString())
			Return err
		ElseIf CInt(result.getResult()) = 0 Then
			Continue For
		End If
	End If
	
	'5-2.取出所有 Assignments
	assignments = inn.newItem("Assignment", "get")
	assignments.setAttribute("select", "id, assignment_type, assignee, parameter, is_required, for_all_members, voting_weight, escalate_to")
	assignments.setProperty("source_id", aa.getID())
	r_assignments = assignments.apply()
	num = r_assignments.getItemCount()
	
	If num = -1 Then
		err = inn.newError(r_assignments.getErrorString())
		Return err
	End If

	For j=0 To num-1
		assignment = r_assignments.getItemByIndex(j)
		ass_type = assignment.getProperty("assignment_type")
		
		If ass_type = "Identity" Then '5-3.Add an Identity To this Activity
			If Not assignment.node.selectSingleNode("//assignee") Is Nothing Then   '若 assignment_type = identity 時， assignee 必須有值
				ass = Me.newItem("Activity Assignment","add")
				ass.setAttribute("leaveLocked","0")
				ass.setProperty("source_id", Me.getID())
				ass.setProperty("related_id", assignment.getProperty("assignee"))
				ass.setProperty("is_required", assignment.getProperty("is_required"))
				ass.setProperty("for_all_members", assignment.getProperty("for_all_members"))
				ass.setProperty("voting_weight", assignment.getProperty("voting_weight"))
				ass.setProperty("escalate_to", assignment.getProperty("escalate_to"))
				r_ass = ass.apply()
				
				If r_ass.isError() Then
					err = inn.newError(r_ass.getErrorString())
					Return err
				End If
			Else
				err = inn.newError("If assignment_type is Identity, then assignee must been filled.")
				Return err
			End If
		ElseIf ass_type = "Manager" Then ' 5-4.Add a Manager to this Activity
			'取出階層
			If (Not assignment.getProperty("parameter") Is Nothing) Then
				If IsNumeric(assignment.getProperty("parameter")) = False Then	
					err = inn.newError("If assignment_type is Manager, then parameter must be a integer.")
					Return err
				Else
					level = CInt(assignment.getProperty("parameter"))
				End If
			Else
				level = 1
			End If
	
			'取出 Manager	
			While level > 0 
				org = inn.newItem("Organization", "get")
				oi = inn.newItem("Organization Identity", "get")
				oi.setProperty("related_id", creator_id)
				org.addRelationship(oi)
				r_org = org.apply()
				
				If r_org.isError() Then
					identity = inn.getItemByID("Identity", creator_id)
					err = inn.newError("Can not find any organization that the identity belong to. Identity=[" & identity.getProperty("name") & "]")
					Return err
				End If
			
				If Not r_org.node.selectSingleNode("//manager")  Is Nothing Then
					level = level -1
					
					If level > 0 Then
						creator_id = r_org.getProperty("manager")    
					Else
						manager_id = r_org.getProperty("manager")
					End If
				Else
					err = inn.newError(r_org.getProperty("name") & " : does not define the manager.")
					Return err
				End If
			
				org = Nothing
				r_org = Nothing
			End While
	
			'將 Manager 加入 Workflow
			If manager_id <> "" Then	
				ass = Me.newItem("Activity Assignment","add")
				ass.setAttribute("leaveLocked","0")
				ass.setProperty("source_id", Me.getID())
				ass.setProperty("related_id", manager_id)
				ass.setProperty("is_required", assignment.getProperty("is_required"))
				ass.setProperty("for_all_members", assignment.getProperty("for_all_members"))
				ass.setProperty("voting_weight", assignment.getProperty("voting_weight"))
				ass.setProperty("escalate_to", assignment.getProperty("escalate_to"))
				r_ass = ass.apply()
				
				If r_ass.isError() Then
					err = inn.newError(r_ass.getErrorString())
					Return err
				End If
			Else
				err = inn.newError(r_org.getProperty("name") & " : does not define the manager.")
				Return err
			End If
	
		ElseIf ass_type = "Field" Then
			If Not assignment.getProperty("parameter") Is Nothing Then
				field_name = assignment.getProperty("parameter")
				
				If Not controlledItem.getProperty(field_name) Is Nothing Then
					manager_id = controlledItem.getProperty(field_name)
					
					ass = Me.newItem("Activity Assignment","add")
					ass.setAttribute("leaveLocked","0")
					ass.setProperty("source_id", Me.getID())
					ass.setProperty("related_id", manager_id)
					ass.setProperty("is_required", assignment.getProperty("is_required"))
					ass.setProperty("for_all_members", assignment.getProperty("for_all_members"))
					ass.setProperty("voting_weight", assignment.getProperty("voting_weight"))
					ass.setProperty("escalate_to", assignment.getProperty("escalate_to"))
					r_ass = ass.apply()
					
					If r_ass.isError() Then
						err = inn.newError(r_ass.getErrorString())
						Return err
					End If
				End If
			Else
				err = inn.newError("If assignment_type Is Field, then parameter must be filled.")
				Return err
			End If
		End If
	
		' Change the Activity text (just so a change is visible in the InBasket)
		act = Me.newItem(Me.GetType(),"edit")
		act.setID(Me.getID())
		act.setProperty("message",  "Assignments Added")
		r_act = act.apply()
		
		If r_act.isError() Then
			err = inn.newError(r_act.getErrorString())
			Return err
		End If
	Next
Next 
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='AddAssignmentsToWFWithCriteria2' and [Method].is_current='1'">
<config_id>52B9425C2EC04DEDA4FD30E5FAB4731F</config_id>
<name>AddAssignmentsToWFWithCriteria2</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
