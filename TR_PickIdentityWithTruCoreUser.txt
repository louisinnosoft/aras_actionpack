string loginName = this.getProperty("login_name") ?? this.getPropertyAttribute("body", "login_name");
string identity = this.getProperty("identityid") ?? this.getPropertyAttribute("body", "identityId");
if (string.IsNullOrEmpty(loginName))
{
	throw new ArgumentException("Login Name");
}
if (string.IsNullOrEmpty(identity))
{
	throw new ArgumentException("Identity");
}

Aras.Server.FileExchange.TrubiquityCoreConnector.TruFileExchangeService ts = 
	(Aras.Server.FileExchange.TrubiquityCoreConnector.TruFileExchangeService)Aras.Server.FileExchange.FileExchangeServiceFactory.CreateExchangeService("Aras.TrubiquityTRUAras");

Aras.Server.FileExchange.TrubiquityCoreConnector.TruCoreUser user = ts.Location.GetUser(loginName);
if (user == null)
{
	return this.getInnovator().newError("Can't find any Trubiquity user with login name = '" + loginName + "'");
}
this.setProperty("first_name", user.FirstName);
this.setProperty("last_name", user.LastName);
this.setProperty("trucoreuserid", user.Id.ToString());

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='TR_PickIdentityWithTruCoreUser' and [Method].is_current='1'">
<config_id>19E0C6D2FE3F4564A41542D9BA0AB4F6</config_id>
<name>TR_PickIdentityWithTruCoreUser</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
