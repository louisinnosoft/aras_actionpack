'Created Date: 2010/03/03, Creator: Scott

'Purpose：Promote from 未發行 or 進行中 to 發行

'System.Diagnostics.Debugger.Break() 

Dim inn As Innovator = Me.getInnovator()
Dim err As Item
Dim result As Item

result = inn.applyMethod("AffectedPartPromote","<ID>" + Me.getID() + "</ID><State>" + Me.getProperty("state") + "</State><FromState></FromState><ToState>發行</ToState><RelName>EC Part</RelName>")

If result.isError() Then
	err = inn.newError("Fail to promote part to 發行. " + result.getErrorString() + " " + result.getErrorDetail())
	Return err 
End If
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='ECPromotePartToRelease' and [Method].is_current='1'">
<config_id>C6BE0D9EF3E64A6ABD1F6AAB23249636</config_id>
<name>ECPromotePartToRelease</name>
<comments>ActionPack</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
