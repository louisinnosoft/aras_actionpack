var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
if (workerFrame && workerFrame['onPaste_specialCommand']) {
	workerFrame['onPaste_specialCommand']();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwmm_onPaste_special' and [Method].is_current='1'">
<config_id>98FD9E3828E2482B9D193E337B1672A0</config_id>
<name>cui_default_mwmm_onPaste_special</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
