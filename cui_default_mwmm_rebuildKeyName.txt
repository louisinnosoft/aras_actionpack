var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
aras.prompt(aras.getResource('', 'mainmenu.rebuild_keyed_name_prop'), workerFrame.itemTypeName).then(function(itemTypeNames) {
	var soapController = new topWindow.SoapController(onRebuildKeyedNamesComplete);
	soapController['custom_statusId'] = topWindow.aras.showStatusMessage('status', ' Rebuilding Keyed Names ...', '../images/Progress.gif');
	topWindow.aras.soapSend('rebuildKeyedName', '<ItemTypes>' + itemTypeNames + '</ItemTypes>', undefined, undefined, soapController);
});

function onRebuildKeyedNamesComplete(res) {
	topWindow.aras.clearStatusMessage(this['custom_statusId']);
	if (res.getFaultCode() !== 0) {
		topWindow.aras.AlertError(topWindow.aras.getResource('', 'mainmenu.rebuilding_kn_failed', res.getFaultString()));
		return false;
	}

	topWindow.aras.AlertSuccess(topWindow.aras.getResource('', 'mainmenu.kn_rebuilt'));
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwmm_rebuildKeyName' and [Method].is_current='1'">
<config_id>0D528618E84D477DA265D63ECB0F2692</config_id>
<name>cui_default_mwmm_rebuildKeyName</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
